#!/bin/sh

project_dir=$(dirname "$0")

printf "Name of project: "
read -r project_name

printf "Is this a library (y/n): "
read -r is_lib

if [ "$is_lib" = "y" ]; then
    cargo new --lib "$project_name"
elif [ "$is_lib" = "n" ]; then
    cargo new "$project_name"
else
    printf "Invalid response\n\n"
    exit 1
fi

printf "Git repository: "
read -r git_repo

printf "Space separated list of all maintainers (black if none): "
read -r maintainers

cp "$project_dir"/data/* .
for file in "$project_dir"/data/*; do
    sed -i "s/<project_name>/$project_name" "$(basename "$file")"
    sed -i "s/<git_repo>/$git_repo" "$(basename "$file")"
    sed -i "s/<maintainers>/$maintainers" "$(basename "$file")"
done
rm -rf "$project_dir"/data/

mv "$project_name"/* .
mv "$project_name"/.* .
rm -rf "$project_name"

sed -i 's/^version.*$/version = "0.0.1"/' Cargo.toml
{
    echo "color-eyre = \"0.6\""
    echo "anyhow = \"1.0\""
    echo "tokio = { version = \"1\", features = [\"full\"] }"
    echo "serde = { version = \"1.0\", features = [\"derive\"] }"
} >> Cargo.toml