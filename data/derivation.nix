{ lib, fetchFromGitHub, rustPlatform }:

rustPlatform.buildRustPackage rec {
  pname = "<project_name>";
  version = "0.0.1";
  
  src = fetchgit {
    url = "<git_repo>";
    rev = "v0.0.1";
    hash = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
  };
  
  cargoHash = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
  
  meta = with lib; {
    description = "<project_name>";
    homepage = "<git_repo>";
    license = licenses.unlicense;
    maintainers = with maintainers; [
      <maintainers>
    ];
  };
}