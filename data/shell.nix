{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  name = "<project_name>";
  packages = with pkgs; [
    rustc
    cargo
    bacon
  ];
}